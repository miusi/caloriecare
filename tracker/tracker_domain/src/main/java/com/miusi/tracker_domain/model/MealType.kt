package com.miusi.tracker_domain.model

sealed class MealType(val name: String) {
    object Breakfast : MealType("breakfast")
    object Lunch : MealType("launch")
    object Dinner : MealType("dinner")
    object Snack : MealType("snack")

    companion object {
        fun fromString(name: String): MealType {
            return when (name) {
                "breakfast" -> Breakfast
                "launch" -> Lunch
                "dinner" -> Dinner
                "snack" -> Snack
                else -> Breakfast
            }
        }

    }
}
