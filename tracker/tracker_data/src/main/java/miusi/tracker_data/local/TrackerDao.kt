package miusi.tracker_data.local

import androidx.room.*
import kotlinx.coroutines.flow.Flow
import miusi.tracker_data.local.entity.TrackedFoodEntity

@Dao
interface TrackerDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTrackedFood(trackerFoodEntity: TrackedFoodEntity)

    @Delete
    suspend fun deleteTrackedFood(trackerFoodEntity: TrackedFoodEntity)

    @Query(
        """
            SELECT *
            FROM trackedfoodentity
            WHERE dayOfMonth = :day AND month = :month AND year = :year
        """
    )
    // Not suspend because we're using flow to hear changes
    fun getFoodsForDate(day: Int, month: Int, year: Int): Flow<List<TrackedFoodEntity>>
}