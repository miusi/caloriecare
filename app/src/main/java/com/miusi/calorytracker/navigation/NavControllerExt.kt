package com.miusi.calorytracker.navigation

import androidx.navigation.NavController
import com.miusi.core.util.UiEvent

fun NavController.navigate(event: UiEvent.Navigate) {
    this.navigate(event.route)
}